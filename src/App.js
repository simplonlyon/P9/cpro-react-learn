import React from 'react';
import './App.css';
import First from './components/First';
import CounterHook from './components/CounterHook';
// import Modal from './components/Modal';
import DogManager from './components/dog/DogManager';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Nav from './components/Nav';
import OneDog from './components/dog/OneDog';
import NotFound from './components/NotFound';

function App() {
  return (
    <Router>
      <h1>Mon Application</h1>
      <p>coucou tout le monde</p>
      <Nav />
      <Switch>
        <Route exact path="/">
          <First />
        </Route>
        <Route path="/counter">
          <CounterHook />
        </Route>
        <Route path="/dog-manager">
          <DogManager />
        </Route>
        <Route path="/dog/:id">
          <OneDog />
        </Route>
        <Route  path="*" component={NotFound} />
      </Switch>
      {/* <Modal onOk={() => console.log('bloup')} />  */}
    </Router>
  );
}

export default App;
