
import axios from "axios";

const url = 'http://localhost:4000/dog';

export async function findDogs() {
    const response = await axios.get(url);
    return response.data;
}

export async function findDog(id) {
    const response = await axios.get(url + '/' + id);
    return response.data;

}

export async function addDog(dog) {
    const response = await axios.post(url, dog);
    return response.data;
}

export async function deleteDog(id) {

    await axios.delete(url + '/' + id);
    return true;
}