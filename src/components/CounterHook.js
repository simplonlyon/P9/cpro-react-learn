import React, {useState} from "react";

export default function CounterHook() {
    const [count, setCount] = useState(0);
    
    const increment = () => setCount(count+1);

    return (
        <section>
            <button onClick={() => setCount(count-1)}>-</button>
            {count}
            <button onClick={increment}>+</button>
            <button onClick={() => setCount(0)}>reset</button>
        </section>
    )
}
