import React from "react";

export default function Dog({dog}) {

    return (
    <article>
        <h3>{dog.name}</h3>
        <p>A {dog.age} year old {dog.breed}</p>
    </article>
    );
}