import React, { useState } from "react";
import Dog from "./Dog";

import './DogList.css';
import { Link } from "react-router-dom";


export default function DogList({ dogs, onAction }) {
    const [selected, setSelected] = useState([]);

    const select = dog => {
        if(selected.includes(dog)) {
            setSelected(selected.filter(item => dog.id !== item.id));
        }else {
            setSelected([...selected, dog]);
        }
    }


    return (
        <section>
            <button onClick={() => onAction(selected)}>Delete Selected</button>
            {dogs.map(dog =>
                <div className={selected.includes(dog) ? 'selected':''} key={dog.id}>
                    <Dog dog={dog} />
                    <Link to={'/dog/'+dog.id}>See Details</Link>
                    <button onClick={() => select(dog)}>
                        {selected.includes(dog) ? 'Unselect':'Select'}
                    </button>
                </div>
            )}
        </section>
    );
}