import React, { useState } from 'react';

const initialState = {
    name: '',
    age: 0,
    breed: ''
};

export default function DogForm({onSubmit}) {
    const [dog, setDog] = useState(initialState);

    const handleChange = event => {
        setDog({...dog, [event.target.name]:event.target.value});
    };

    const handleSubmit = event => {
        event.preventDefault();
        onSubmit(dog);
        setDog(initialState)
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>Name : </label>
            <input name="name" type="text" onChange={handleChange} value={dog.name} />

            <label>Breed : </label>
            <input name="breed" type="text" onChange={handleChange} value={dog.breed} />

            <label>Age : </label>
            <input name="age" type="number" onChange={handleChange} value={dog.age} />

            <button>Submit Dog</button>
        </form>
    );
}