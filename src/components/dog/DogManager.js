import React, { useState, useEffect } from "react";
import DogForm from "./DogForm";
import DogList from "./DogList";
import { findDogs, addDog, deleteDog } from "../../dog-service";


export default function DogManager() {
    const [dogs, setDogs] = useState([]);

    
    useEffect(() => {
        findDogs().then(data => setDogs(data));
    }, []);

    const postDog = dog => {
        addDog(dog).then(data => setDogs([...dogs, data]));
        
    };
    const delDog = selected => {
        const promises = [];
        for (const dog of selected) {
            promises.push(deleteDog(dog.id));
        }
        Promise.all(promises).then(() => {
            setDogs(dogs.filter(item => !selected.includes(item)));
        });
    };
    
    return (
        <section>
            <h2>Dog Management</h2>
            <DogForm onSubmit={postDog} />
            <DogList dogs={dogs} onAction={delDog}/>
        </section>
    );

}