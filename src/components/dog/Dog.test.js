import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Dog from './Dog';

test('component should render', () => {
    let dog = {id:1, name:'test', breed:'breed', age:1};
    const comp = render(<Dog dog={dog} />);
    expect(comp.container.textContent).toMatch('test');
    expect(comp.container.textContent).toMatch('breed');
    expect(comp.container.textContent).toMatch('1');

});