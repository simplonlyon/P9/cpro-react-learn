import React, { useEffect, useState } from "react";
import { useParams, useHistory, Link } from "react-router-dom";
import { findDog } from "../../dog-service";
import Dog from "./Dog";


export default function OneDog() {
    const history = useHistory();
    const {id} = useParams();
    const [dog, setDog] = useState(null);
    
    useEffect(() => {
        findDog(id)
        .then(dog => setDog(dog))
        .catch(() => history.push("/not-found"));
    }, [id]);

    return (
        <>
            {dog ? <Dog dog={dog} /> : <p>Loading...</p>}
        </>
    )
}