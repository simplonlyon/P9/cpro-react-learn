import React from 'react';
import { Link } from 'react-router-dom';

export default function Nav() {
    return (
        <nav>
            <ul>
                <li><Link to="/">First</Link></li>
                <li><Link to="/counter">Counter</Link></li>
                <li><Link to="/dog-manager">Dog Manager</Link></li>
            </ul>

        </nav>
    );
}