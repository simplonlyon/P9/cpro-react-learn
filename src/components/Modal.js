import React, { useState } from "react";
import './Modal.css';

export default function Modal({ content = 'No Content', onOk = () =>{} }) {
    const [showModal, setShowModal] = useState(false);

    const toggleModal = () => {
        if (showModal) {
            setShowModal(false);
        } else {
            setShowModal(true);
        }
    }

    return (
        <>
            <button onClick={toggleModal}>Toggle</button>
            {showModal &&
                <section className="modal">
                    {content}
                    <button onClick={toggleModal}>X</button>
                    <div>
                        <button onClick={onOk}>Ok</button>
                    </div>
                </section>
            }
        </>
    );
}