import React from "react";


export default class Counter extends React.Component {
    state = {
        count: 0
    }

    increment = () => {
        this.setState({
            count: this.state.count + 1
        });
    }

    decrement = () => {
        this.setState({
            count: this.state.count - 1
        });
    }

    reset = () => {
        this.setState({
            count: 0
        });
    }

    render() {
        return (
            <section>
                <button onClick={this.decrement}>-</button>
                {this.state.count}
                <button onClick={this.increment}>+</button>
                <button onClick={this.reset}>reset</button>
            </section>
        )
    }
}