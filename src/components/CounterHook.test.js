import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CounterHook from './CounterHook';


test('component should render', () => {
    
    const comp = render(<CounterHook />);
    expect(comp.container.textContent).toMatch('0');
    const btnP = comp.getByText('+');
    const btnM = comp.getByText('-');
    const btnR = comp.getByText('reset');
    
    fireEvent.click(btnP);
    expect(comp.container.textContent).toMatch('1');
    fireEvent.click(btnM);
    expect(comp.container.textContent).toMatch('0');
    fireEvent.click(btnP);
    fireEvent.click(btnP);
    fireEvent.click(btnR);
    expect(comp.container.textContent).toMatch('0');



});