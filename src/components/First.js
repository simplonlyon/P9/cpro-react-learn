import React from "react";

export default class First extends React.Component  {
    state = {
        message: 'My message'
    }
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         message: 'My message'
    //     };
        
    // }

    
    changeMessage = () => {
        this.setState({
            message: 'Other Message'
        });
    }

    render() {
        return (
            <section>
                <p>First component</p>
                <p>{this.state.message}</p>
                <button onClick={this.changeMessage}>Change</button>
            </section>
        );
    }
}